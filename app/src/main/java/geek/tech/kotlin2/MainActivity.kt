package geek.tech.kotlin2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import geek.tech.kotlin2.data.room.Constant
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val viewModel:MainViewModel by viewModels()
    private val mainRepo: MainRepo by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.nameLiveData.observe(this){
            text.text = it
            Log.e("NAme",it)
        }

        viewModel.nameLiveData.observeForever(){
            text.text = it
        }

        getStrings("sdsd","shjdjksd","jsdjksdjk")

        viewModel.getStudents()
        viewModel.quizLiveData.observe(this){

        }
    }

    private fun getStrings(vararg s:String){
        for (s1 in s){

        }
    }


}