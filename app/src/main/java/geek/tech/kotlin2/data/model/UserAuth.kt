package geek.tech.kotlin2.data.model

import com.google.gson.annotations.SerializedName

data class UserAuth(
        @SerializedName("username") val un:String,
        val password:String
)
