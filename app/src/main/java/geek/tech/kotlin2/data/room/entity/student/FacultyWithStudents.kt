package geek.tech.kotlin2.data.room.entity.student

import androidx.room.Embedded
import androidx.room.Relation
import geek.tech.kotlin2.data.room.entity.faculty.Faculty

data class FacultyWithStudents(
    @Embedded
    val faculty: Faculty,

    @Relation(
            parentColumn = "id",
            entityColumn = "faculty_id"
    )

    val students: List<Student>
)