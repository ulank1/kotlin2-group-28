package geek.tech.kotlin2.data.room.entity.lesson

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity
data class Lesson(
        @PrimaryKey(autoGenerate = true)
        var lesson_id:Int,
        var title: String,
        var admin_id:Int
)
