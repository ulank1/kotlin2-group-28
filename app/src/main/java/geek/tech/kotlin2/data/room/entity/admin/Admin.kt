package geek.tech.kotlin2.data.room.entity.admin

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity
data class Admin(
        @PrimaryKey(autoGenerate = true)
        var id:Int,
        var name: String,
)
