package geek.tech.kotlin2.data.room.entity.lesson

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Junction
import androidx.room.Relation
import geek.tech.kotlin2.data.room.entity.student.Student

@Entity(primaryKeys = ["lesson_id", "student_id"])
data class LessonStudentCrossRef(
        val lesson_id: Int,
        val student_id: Int
)

data class LessonWithStudents(
        @Embedded val lesson: Lesson,
        @Relation(
                parentColumn = "lesson_id",
                entityColumn = "student_id",
                associateBy = Junction(LessonStudentCrossRef::class)
        )
        val student: List<Student>
)


