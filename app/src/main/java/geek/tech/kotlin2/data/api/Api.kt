package geek.tech.kotlin2.data.api

import com.google.gson.JsonObject
import geek.tech.kotlin2.data.model.UserAuth
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface Api {
    @GET("api.php/{category_id}/quiz")
    suspend fun getQuiz(
            @Path("category_id") category_id: Int,
            @Query("category") category: ArrayList<Int>,
            @Query("amount") amount: Int,
    ): Response<Any>

    @FormUrlEncoded
    @POST("api/v1/login")
    fun login(
            @Field("username") username: String,
            @Field("password") password: String
    )

    @POST("api/v1/login")
    fun login(
            @Body username: UserAuth
    )
    @Headers("Authorization: token HJDGljsbuyvadcjknsjbhcjdvc")
    @PUT("api/ad/{id}")
    fun updateAd(
            @Header("Authorization") token:String,
            @Path("id") id: Int,
            @Field("title") title: String,
            @Field("image") image: String,
            @Field("price") price: Int,
//            @Field("amount") amount:Int
    )

    @PATCH("api/ad/{id}")
    fun updateAd(
            @Path("id") id: Int,
            @Field("image") image: String,
            @Field("price") price: Int
    )

    @DELETE("api/ad/{id}")
    fun deleteAd(
            @Path("id") id: Int
    )

    @GET
    fun filter(@Url url: String)

    //"price[]=100&price[]=105&model=BMW&brand=X5&millage[]=1000&millage[]=1000000&category"

    @GET("api/filter")
     suspend fun getFilterAd(
            @Query("category",encoded = true) category: String,
            @QueryMap(encoded = true) params:Map<String,Any>

    ):Response<Any>

     @Multipart
     @POST("api/v1/image")
     fun postImage(
             @Part image: MultipartBody.Part
     ):Response<Any>
}