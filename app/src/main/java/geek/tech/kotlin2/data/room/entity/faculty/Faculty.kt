package geek.tech.kotlin2.data.room.entity.faculty

import androidx.room.Entity
import androidx.room.PrimaryKey
@Entity
data class Faculty(
        @PrimaryKey(autoGenerate = true)
        var id:Int,
        var title: String,
        var address: String,
)
