package geek.tech.kotlin2.data.room.dao

import androidx.room.Embedded
import androidx.room.Relation
import geek.tech.kotlin2.data.room.entity.admin.Admin
import geek.tech.kotlin2.data.room.entity.faculty.Faculty
import geek.tech.kotlin2.data.room.entity.lesson.Lesson
import geek.tech.kotlin2.data.room.entity.lesson.LessonWithStudents

data class AdminWithLessonAndStudents(
    @Embedded
    val admin: Admin,
    @Relation(
            entity= Lesson::class,
            parentColumn = "id",
            entityColumn = "admin_id"
    )
    val lessons: List<LessonWithStudents>
)