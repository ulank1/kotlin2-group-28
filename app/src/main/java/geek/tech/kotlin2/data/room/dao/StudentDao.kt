package geek.tech.kotlin2.data.room.dao

import androidx.room.*
import geek.tech.kotlin2.data.room.entity.lesson.LessonWithStudents
import geek.tech.kotlin2.data.room.entity.student.Student
import geek.tech.kotlin2.data.room.entity.student.FacultyWithStudents

@Dao
interface StudentDao {
    @Query("SELECT * FROM student")
    fun getAll(): List<Student>

    @Query("SELECT full_name, age FROM student")
    fun getNames(): List<String>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg student: Student)

    @Delete
    fun delete(student: Student)

    @Query("SELECT * FROM student WHERE age >= :minAge")
    fun loadAllUsersOlderThan(minAge: Int): List<Student>

    @Query("SELECT * FROM student WHERE full_name LIKE :search OR age IN (:age)")
    fun findUserWithName(search: String,age: List<Int>): List<Student>

//    @Transaction
//    @Query("SELECT * FROM faculty")
//    fun getUsersAndLibraries(): List<FacultyWithStudents>
//
//    @Transaction
//    @Query("SELECT * FROM lesson")
//    fun getPlaylistsWithSongs(): List<LessonWithStudents>
//    @Transaction
//    @Query("SELECT * FROM student")
//    fun getStudentsWithLessons(): List<StudentWithLessons>

    @Transaction
    @Query("SELECT * FROM admin")
    fun getStudentsWithLessons(): List<AdminWithLessonAndStudents>

}