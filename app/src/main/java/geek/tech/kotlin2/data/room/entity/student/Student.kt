package geek.tech.kotlin2.data.room.entity.student

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import geek.tech.kotlin2.data.room.entity.faculty.Faculty

@Entity
data class Student(
    @PrimaryKey(autoGenerate = true)
    var student_id:Int,
    var full_name:String,
    var age:Int
)
