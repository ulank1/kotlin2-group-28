package geek.tech.kotlin2.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import geek.tech.kotlin2.data.room.dao.StudentDao
import geek.tech.kotlin2.data.room.entity.student.Student

@Database(entities = arrayOf(Student::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun studentDao(): StudentDao
}