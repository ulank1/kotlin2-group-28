package geek.tech.kotlin2

import android.app.Application
import geek.tech.kotlin2.di.networkModule
import geek.tech.kotlin2.di.roomModule
import geek.tech.kotlin2.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module

class App:Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@App)
            modules(arrayListOf(roomModule, viewModelModule, networkModule))
        }

    }

}