package geek.tech.kotlin2

import geek.tech.kotlin2.data.api.Api
import geek.tech.kotlin2.data.room.dao.StudentDao
import geek.tech.kotlin2.data.room.entity.student.Student
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import retrofit2.Response

class MainRepo(private val studentDao: StudentDao,private val api:Api) {

    suspend fun getAllStudents():List<Student> {
        var students:List<Student>? = null
        withContext(Dispatchers.IO){
            students = studentDao.getAll()
        }
        return students!!
    }

//    suspend fun getQuiz():Response<Any>?{
//        return try {
//            api.getQuiz()
//        }catch (e:Exception){
//            null
//        }
//    }

    suspend fun getFilterAd(category:Int,params:Map<String,Any>):Response<Any>?{
        return try {
            api.getFilterAd(category,params)
        }catch (e:Exception){
            null
        }
    }

    suspend fun postImage(image:MultipartBody.Part):Response<Any>?{
        return try {
            api.postImage(image)
        }catch (e:Exception){
            null
        }
    }

}