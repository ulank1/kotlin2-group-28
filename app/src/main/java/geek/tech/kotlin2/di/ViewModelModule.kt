package geek.tech.kotlin2.di

import geek.tech.kotlin2.MainRepo
import geek.tech.kotlin2.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val  viewModelModule = module {

    viewModel { MainViewModel(get()) }
}