package geek.tech.kotlin2.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import geek.tech.kotlin2.data.room.AppDatabase
import geek.tech.kotlin2.data.room.dao.StudentDao
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val roomModule = module {

    fun getDatabase(applicationContext:Context) =
     Room.databaseBuilder(
        applicationContext,
        AppDatabase::class.java, "database-name"
    ).build()
    fun getStudentDao(roomDatabase: AppDatabase):StudentDao{
        return roomDatabase.studentDao()
    }
    single { getDatabase(androidContext()) }
    single { getStudentDao(get()) }
}