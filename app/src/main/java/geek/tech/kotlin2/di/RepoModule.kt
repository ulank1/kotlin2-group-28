package geek.tech.kotlin2.di

import geek.tech.kotlin2.MainRepo
import org.koin.dsl.module

val repoModule = module {
    single { MainRepo(get(),get()) }
}