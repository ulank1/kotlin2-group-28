package geek.tech.kotlin2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import geek.tech.kotlin2.data.room.entity.student.Student
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


class MainViewModel(private val vmRepo: MainRepo):ViewModel() {
    private val nameLiveDataPrivate:MutableLiveData<String> = MutableLiveData()
    val nameLiveData:LiveData<String> = nameLiveDataPrivate

    private val quizLiveDataPrivate:MutableLiveData<Any> = MutableLiveData()
    val quizLiveData:LiveData<Any> = quizLiveDataPrivate

    private val studentLiveDataPrivate:MutableLiveData<List<Student>> = MutableLiveData()
    val studentLiveData:LiveData<List<Student>> = studentLiveDataPrivate
    fun getName(){
       nameLiveDataPrivate.value = "Kotlin 2"
    }

    fun getStudents(){
        viewModelScope.launch {
            studentLiveDataPrivate.value = vmRepo.getAllStudents()
        }
    }

//    fun getQuiz(){
//        viewModelScope.launch {
//            val response  = vmRepo.getQuiz()
//            if (response!=null){
//                if (response.isSuccessful){
//                    quizLiveDataPrivate.value = response.body()
//                }
//            }
//        }
//    }

    fun getFilterAd(){

        val hashMap = HashMap<String, Any>()
        hashMap["price"] = listOf(100, 105)
        hashMap["model"] = "BMW"
        hashMap["brand"] = "X5"
        hashMap["millage"] = listOf(1000, 1000000)
        viewModelScope.launch {
            vmRepo.getFilterAd(1, hashMap)
        }

    }

    fun postImage(path: String){
        val file = File(path)
        val requestFile: RequestBody = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)

        val body: MultipartBody.Part = MultipartBody.Part.createFormData("image", file.name, requestFile)
        viewModelScope.launch {
            vmRepo.postImage(body)
        }
    }


}